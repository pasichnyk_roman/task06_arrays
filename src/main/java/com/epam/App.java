package com.epam;

import com.epam.PriorityQueue.CustomPriorityQueue;
import com.epam.PriorityQueue.Students;

public class App {

    public static void main(String[] args) {


        CustomPriorityQueue<Students> queue = new CustomPriorityQueue<>();

        queue.add(new Students("Anton", 15));
        queue.add(new Students("Andrii", 18));
        queue.add(new Students("Roman", 11));
        queue.add(new Students("Ihor", 11));
        queue.add(new Students("Taras", 23));
        queue.add(new Students("Volodya", 126));
        queue.add(new Students("Volodya", 1126));
        queue.add(new Students("Volodya", 12166));
        queue.add(new Students("Volodya", 126));
        queue.add(new Students("Volodya", 61));
        queue.add(new Students("Volodya", 12));

        queue.print();
//

//        System.out.println(Integer.compare(1, 2));


    }

}


