package com.epam.PriorityQueue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class CustomPriorityQueue<T extends Comparable<T>> {


    private int size = 0;

    public Object[] allItems;


    public CustomPriorityQueue() {
        allItems = new Object[size];

    }



    Comparator<T> comparator = new Comparator<T>() {
        @Override
        public int compare(T o1, T o2) {
            return o1.compareTo(o2);
        }
    };


    public void add(T item) {
        if (item == null) {
            throw new NullPointerException();
        }
        if (size >= allItems.length) {
            increaseSize();
        }

        for (int i = 0; i < allItems.length; i++) {
            if (allItems[i] == null) {
                allItems[i] = item;
                break;
            }
        }
        Object bufObj;
        for (int i = 0; i < allItems.length; i++) {
            for (int j = 0; j < allItems.length - 1; j++) {
                if (Objects.compare((T) allItems[i], (T) allItems[j], comparator) < 0) {
                    bufObj = allItems[i];
                    allItems[i] = allItems[j];
                    allItems[j] = bufObj;
                }
            }

        }

    }


    public void increaseSize() {
        int oldCapacity = allItems.length;
        int newCapacity = oldCapacity + 1;
        allItems = Arrays.copyOf(allItems, newCapacity);
        size = newCapacity;
    }

    public void print() {
        for (Object obj : allItems) {
            System.out.println(obj);
        }
    }

    public Object peek() {
        return allItems[0];
    }

    public Object pool() {
        Object obj = allItems[0];
        allItems = Arrays.copyOfRange(allItems, 1, size);
        size--;
        return obj;
    }

    public int size() {
        return size;
    }


}
